# Program Menghitung IPK Mahasiswa
Program ini dibuat menggunakan bahasa Assembly

# Deskripsi Aplikasi
Aplikasi yang dapat digunakan untuk menghitung nilai ipk mahasiswa dari semester 1-8,
dimana inputnya yaitu nilai ip persemester mahasiswa dan outputnya adalah ipk yang dihitung berdasarkan
nilai rata-rata ip semester dan dibagi dengan jumlah semester.
[Link Video Presentasi Aplikasi](https://youtu.be/kB4l5jbMOBI)

# Fitur Aplikasi
    * User dapat memasukan jumlah semester yang akan dihitung IPKnya 
    * Jumlah input IP Per semester di generate berdasarkan jumlah semester yang di input oleh user.
    * User dapat menginputkan satu persatu IP per semester

# Cara Running Aplikasi
1. Download Source Code .asm
2. Buka file .asm menggunakan tools emu8086
3. Lakukan compile file menggunakan fitur compile emu8086
4. Enjoy!

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:f536ccbc07eb36f6c59b7d732dda66f0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:f536ccbc07eb36f6c59b7d732dda66f0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file)