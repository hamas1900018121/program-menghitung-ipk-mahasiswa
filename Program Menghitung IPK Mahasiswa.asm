NAME "Program Menghitung IPK Mahasiswa"
.MODEL SMALL
.CODE

ORG 100h

jmp Start

; Deklrasi Variable
ipk  db ?

; Fungsi untuk mencetak pesan
cetak_pesan Macro kata
    lea dx, kata  
    mov ah, 9
    int 21h
endm

; Fungsi untuk mencetak output berupa angka
cetak_output macro angka
    mov al,angka
    cmp al,09h
    je duadigit 
    duadigit:
        xor ax,ax
        mov al,angka
        mov bl,10
        div bl
        mov bh,ah
        mov dl,bh
        or dl,30h
        mov ah,2
        int 21h
endm

; Fungsi untuk mencetak string angka
cetak_angka macro data
    mov dl, data
    mov ah, 2h
    int 21h
endm

; Fungsi untuk meminta input user
input_data macro reg
    cetak_pesan input_user
    cetak_angka reg
    cetak_pesan titikDua

    mov ah, 01h
    int 21h
    sub al, 30h
    add al, ipk
    mov ipk, al
    cetak_pesan line           

endm

; Message yang dipakai untuk tampilan program
Welcome db 'Created By', 13, 10
        db '=> Hamas Ardyan Prasetyo (1900018121) DSK C 2021', 13, 10, 13, 10
        db '+======================================+', 13, 10
        db '+       SELAMAT DATANG DIPROGRAM       +', 13, 10
        db '+       MENGHITUNG IPK MAHASISWA       +', 13, 10
        db '+======================================+', 13, 10
        db '+ Masukan Jumlah Semester : $'

spasi       db '', 13, 10, 13, 10
            db '$'            
input_user  db 13, 10, '+ IP semester $'
titikDua    db ': $'

line        db 13, 10, '+====================$'

output              db '+======================================+', 13, 10
                    db '+           IPK, $' 
output2             db ' Semester: $', 13, 10
line2       db 13, 10, '+======================================+$'  

Start:
    cetak_pesan Welcome ; Mencetak Pesan awal program
    mov ah, 01h         ; Perintah input user
    int 21h

    mov bh, al          ; Input user dimasukan ke register bh
    sub al, 30h         ;
    mov bl, al          ; Input ser dimasukan ke bl, untuk hitung rata-rata

    mov al, 00          ; Inisialisasi register al, dgn value 00
    mov ipk, al         ; Salin value dari register al ke var ipk

    cmp bh, '1'         ; Apabila input user 1
    je Sem_1            ; Jalankan statement pada Sem_1
    
    cmp bh, '2'         ; Apabila input user 2
    je Sem_2            ; Jalankan statement pada Sem_2

    cmp bh, '3'         ; Apabila input user 3
    je Sem_3            ; jalankan statement pada Sem_3

    cmp bh, '4'         ; Apabila input user 4
    je Sem_4            ; jalankan statement pada Sem_4

    cmp bh, '5'         ; Apabila input user 5
    je Sem_5            ; jalankan statement pada Sem_5

    cmp bh, '6'         ; Apabila input user 6
    je Sem_6            ; jalankan statement pada Sem_6

    cmp bh, '7'         ; Apabila input user 7
    je Sem_7            ; jalankan statement pada Sem_7

    cmp bh, '8'         ; Apabila input user 8
    je Sem_8            ; jalankan statement pada Sem_8
    
Sem_1: 
    cetak_pesan spasi       ; Cetak pesan dari variable spasi
    input_data '1'          ; Input data yang ke-1 
    
    cetak_pesan spasi       ; Cetak pesan dari variable spasi
    cetak_pesan output      ; Cetak pesan dari variable output
    cetak_angka '1'         ; Cetak angka 1
    cetak_pesan output2     ; Cetak pesan dari variabel output2
    cetak_output ipk        ; Cetak nilai ipk
    jmp Finish              ; Lompat ke statement Finish
    
Sem_2:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '2' 
    cetak_pesan output2
    jmp Rata_rata           ; Lompat ke statement Rata-rata

Sem_3:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '3' 
    cetak_pesan output2
    jmp Rata_rata

Sem_4:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    input_data '4'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '4' 
    cetak_pesan output2
    jmp Rata_rata

Sem_5:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    input_data '4'
    input_data '5'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '5' 
    cetak_pesan output2
    jmp Rata_rata

Sem_6:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    input_data '4'
    input_data '5'
    input_data '6'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '6' 
    cetak_pesan output2
    jmp Rata_rata

Sem_7:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    input_data '4'
    input_data '5'
    input_data '6'
    input_data '7'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '7' 
    cetak_pesan output2
    jmp Rata_rata

Sem_8:
    cetak_pesan spasi
    input_data '1'
    input_data '2'
    input_data '3'
    input_data '4'
    input_data '5'
    input_data '6'
    input_data '7'
    input_data '8'
    
    cetak_pesan spasi
    cetak_pesan output
    cetak_angka '8' 
    cetak_pesan output2
    jmp Rata_rata

Rata_rata:
    mov ax, 00      ; Inisialisasi register ax dgn value 00
    mov al, ipk     ; Salin nilai var ipk ke register al
    div bl          ; Membagi register al (jum ipk) dgn register bl (input jum sem)
    add ax, 3030h   ; Tambahkan 3030 ke register ax
    mov dx, ax      ; Salin register ax, ke register dx
    mov ah, 02h     ; Cetak ipk
    int 21h         ; Interupsi untuk cetak ke layar

Finish:
    cetak_pesan line2   ; Cetak pesan dari variabel line2
    int 20h             ; Interupsi untuk menghentikan program
end Start